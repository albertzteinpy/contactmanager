# coding: utf-8
from django import template
from django.core.urlresolvers import reverse
from appcontact.models import *
import string as STrg
from tasks.views import WorkCalendar
from django.utils.safestring import mark_safe
import datetime

register = template.Library()

options = [
            ['Trabajo','Casa','Otro']
]

@register.simple_tag
def alberto(target,selected):
    if str(options[0][target])==str(selected):
        return 'selected="selected"'
    else:
        return ''

@register.simple_tag   
def tagsbyletter():
    a = {x:[] for x in STrg.ascii_uppercase}
    T = Tag.objects.all()
    for x in T:
        a[x.tagname[0]].append({'tag':x.tagname})

    #letter = sorted(set(letter))
    return a

@register.simple_tag
def calendarius(y=None,m=None):
    if not y: y = datetime.datetime.now().year
    if not m: m = datetime.datetime.now().month
    cal = WorkCalendar({}).formatmonth(int(y),int(m))
    return mark_safe(cal)
