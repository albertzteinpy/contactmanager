# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
import simplejson
from django.core import serializers
from contactmanager.forms import *
from appentity.models import * 
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
from django.contrib.auth import logout
import string as STrg
from appcontact.views import abc_tag_list 
from django.db.models import Q
from calendar import HTMLCalendar
from datetime import date
from itertools import groupby
from django.utils.html import conditional_escape as esc
import datetime as dtf
from django.utils.safestring import mark_safe
from contactmanager.forms import *
from tasks.models import * 


class WorkCalendar(HTMLCalendar):
    def __init__(self,tasks):
        super(WorkCalendar,self).__init__()
    def formatday(self, day, weekday):
        if day != 0:
            cssclass = self.cssclasses[weekday]
            if date.today() == date(self.year, self.month, day):
                cssclass += ' today' 
            return self.day_cell(cssclass, day)
        return self.day_cell('noday', '&nbsp;')

    def formatmonth(self, year, month):
        self.year, self.month = year, month
        return super(WorkCalendar, self).formatmonth(year, month)

    def group_by_day(self, workouts):
        field = lambda workout: workout.performed_at.day
        return dict(
            [(day, list(items)) for day, items in groupby(workouts, field)]
        )

    def day_cell(self, cssclass, body):
        thap = '#%s/%s/%s' %(self.month,body,self.year)
        return '<td class="%s"><a href="%s" class="addingdate inline_blocke">%s</a></td>' % (cssclass,thap, body)



@login_required(login_url='/')
def calendar(request):
    yearto = int(request.GET.get('y',dtf.datetime.now().year))
    monthto = int(request.GET.get('m',dtf.datetime.now().month))
    cal = WorkCalendar({}).formatmonth(yearto,monthto)
    return HttpResponse(mark_safe(cal))


def tasking(request):
    forma = FormCreator()
    modelo = Tasking
    formando = forma.form_to_model(modelo)
    datosreci = request.POST.copy()
    datosreci['creatore']=request.user.pk
    formando = formando(datosreci)
    if formando.is_valid():
        tipo = formando.cleaned_data['asigned_string']
        idtipo = formando.cleaned_data['asignedto']
        newtask = formando.save()
        if tipo == 'caso':
            datos = Tasking.objects.filter(asigned_string ='caso',asignedto=idtipo)
            data = serializers.serialize("json", datos)
            return HttpResponse(data, content_type="application/json")
        elif tipo == 'proceso':
            datos = Tasking.objects.filter(asigned_string ='proceso',asignedto=idtipo)
            data = serializers.serialize("json", datos)
            return HttpResponse(data, content_type="application/json")
        else:
            data = {'add':newtask.pk,'data':datosreci}
            data = simplejson.dumps(data)
            return HttpResponse(data)
    else:
        data = formando.errors
        data = simplejson.dumps(data)
        return HttpResponse(data)



@login_required(login_url='/')
def tasklist(request):
   return HttpResponse('a') 
 

