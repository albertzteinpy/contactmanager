# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
import simplejson


CHOICES_TASK = (('0','el día del evento'),
                ('1','1 día antes'),
                ('2','2 dias antes'),
)


class Tasking(models.Model):
    asigned_string = models.CharField(max_length=100)
    asignedto = models.IntegerField(blank=True,null=True)
    date_create = models.DateTimeField(auto_now=True)
    when = models.DateField()
    anytime = models.TimeField(blank=True,null=True)
    reminder = models.CharField(max_length=100,choices=CHOICES_TASK)
    mail_to_send = models.CharField(max_length=200,blank=True,null=True)
    note = models.TextField(blank=True,null=True)
    title_task = models.CharField(max_length=200)
    taskstatus = models.BooleanField(default=0)
    creatore = models.ForeignKey(User)


class Watcher(models.Model):
    taskid = models.ForeignKey(Tasking)
    wathcer = models.TextField()

