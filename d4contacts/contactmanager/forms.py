# -*- encoding: utf-8 -*-
from django.forms import ModelForm
import simplejson
from appcontact.models import *
from appentity.models import *
from django import forms
class FormCreator:
    def __init__(self):
        pass

    def form_to_model(self,modelo):
        if modelo: 
            meta=type('Meta',(),{'model':modelo})
            crearForm=type('modelform',(ModelForm,),{"Meta":meta})
        return crearForm

    def advanced_form_to_model(self,modelo,fields=None,widgets=None,excludes=None):
        meta = type('Meta',(),{"model":modelo,'fields':fields,'widgets':widgets,'exclude':excludes})
        KtemodelfomrKlss = type('modelform',(ModelForm,),{"Meta":meta})
        return KtemodelfomrKlss


class FormatingData:
    def __init__(self):
        pass

    def list_params(self,params):
        myname = params[0]
        myvalues = params[1]
        typesof = params[2]
        listing = []
        for x in myvalues:
            if x:
                listing.append({myname:x,
                'type'+str(myname):typesof[myvalues.index(x)],
                })

        return simplejson.dumps(listing)

    def saveListRelated(self,params,targeted):
        myvalues = params[0]
        mytypes = params[1]
        listing = [{'email':x,
                 'typeofemail':mytypes[myvalues.index(x)]} for x in myvalues]
        EmailList.objects.filter(contacto=targeted.pk).delete()
        for x in listing:
            if x['email']:
                emailsaver= EmailList(contacto=targeted,email=x['email'],typeofemail=x['typeofemail'])
                emailsaver.save()
 
        return listing

    def saveListEmaiLEntity(self,params,targeted):
        myvalues = params[0]
        mytypes = params[1]
        listing = [{'email':x,
                 'typeofemail':mytypes[myvalues.index(x)]} for x in myvalues]
        EntityEmailList.objects.filter(entity_id=targeted.pk).delete()
        for x in listing:
            if x['email']:
                emailsaver= EntityEmailList(entity_id=targeted,email=x['email'],typeofemail=x['typeofemail'])
                emailsaver.save()
 
        return listing


            
#FIELDS_LIST = {
#                'open':forms.CharField('label')
#              }

class DynamicForm:
    def __init__(self):
        pass
    
    def herold(self,params):
        choices = [('','--------seleccione---------------'),]
        if params.typefield == 'option':
            choices += [(x.pk,u'%s'%x.optiontxt) for x in params.optionfields_set.all()]
            field = forms.ChoiceField(label=u'%s'%params.fieldname,choices=choices)
        else:
            field = forms.CharField(label=u'%s'%params.fieldname,max_length=200,required=True)
        
        return field

    def questionarybycase(self,typecase=None):
        if not typecase:
            return None
        q = None #Questionary.objects.filter(casetype=typecase).order_by('qorder')
        fields = {}
        for p in q:
            if p.qtype in 'open':
                fields[str(p.pk)]=forms.CharField(label=u'%s'%p.question,max_length=200,required=True)
            else:
                choicesx = p.options.split('|')
                choices = [(x,x) for x in choicesx]
                fields[u'q_%s'%p.pk]=forms.ChoiceField(label=u'%s'%p.question,choices=choices)
    
        formax = type('Contactame',(forms.Form,),fields)
        
        return formax



    def from_model(self,modeloQ=None):
        fields = {}
        if modeloQ:
            for x in modeloQ:
                fields[u'q_%s'%x.pk] = self.herold(x) 
            forma = type('Formextra',(forms.Form,),fields)

            return forma
        else:
            return ''


class Formcontact(forms.Form):
    CHOICES_CONTACT = [('Email','Email'),
                        ('Tel. Móvil','Tel. Móvil'),
                        ('Tel. Fijo','Tel. Fijo'),
                        ('Tel. Recados','Tel. Recados'),
                        ]
    CHOICES_WHERE = [('Trabajo','Trabajo'),
                     ('Casa','Casa'),
                     ('Otro','Otro')
                    ]
    CHOICES_REDSOCIAL = [('Facebook','Facebook'),
                         ('Twitter','Twitter'),
    
                        ]

    typeof = forms.ChoiceField(label=u'Forma de contactar',choices=CHOICES_CONTACT)
    contactinfo = forms.CharField(label=u'',max_length=200)
    whereis = forms.ChoiceField(label=u'',choices=CHOICES_WHERE) 
    redsociallist = forms.ChoiceField(choices=CHOICES_REDSOCIAL)
    redsocial = forms.CharField(max_length=200)
    direccion = forms.CharField(max_length=200)
    whereisdir = forms.ChoiceField(label=u'',choices=CHOICES_WHERE)




