from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',
    (r'^', include('appcontact.urls')),
    (r'^', include('appentity.urls')),
    (r'^', include('tasks.urls')),
    url(r'^admin/',include(admin.site.urls)),        
)


