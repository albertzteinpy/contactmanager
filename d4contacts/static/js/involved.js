$.fn.remove_err = function()
{
    $(this).focus(function(){
        $(this).siblings('.err:first').remove();     
    });
   
}


params_to_dropdowns = [{'changer':'#id_state','victim':'#id_municipio','url':'/location','param':'pk'},
                        {'changer':'#id_birth_state','victim':'#id_birth_mun','url':'/location','param':'pk'}
                      ];

function changers(params)
{
    $.each(params,function(x,y){
        $(y.changer).change(function(){
            var valor = $(this).val();
            $(y.victim).html('<option>Espere un momento por favor</option>');
            url = y.url+'?'+y.param+'='+valor;
            defaultvalue = '<option value="">------------Municipio-----------</option>';
            $(y.victim).html(defaultvalue);
            $.getJSON(url,{},function(r){
                $.each(r,function(k,v){
                    $(y.victim).append('<option value="'+v.municipio+'">'+v.municipio+'</option>');    
                    
                });
                

            });
        });    
    });
   
}


function sendit(e)
{
    e.preventDefault();
    // EXTRACTING DE FORM DATA
    var data = $(this).serializeArray();
    var url = $(this).attr('action');
    var method_is = $(this).attr('method');

    $.ajax({url:url,
            type:method_is,
            data:data,
            dataType:'json',
            success:function(response){
                $('.err').remove();
                $.each(response,function(x,y){
                    if(response.success)
                    {
                        alert('información guardada con exito');
                    }
                    else
                    {
                        $('#id_'+x).after('<span class=" inline_blocke err">'+y+'</span>').remove_err();
                    }
                });    
            }
    });
}




