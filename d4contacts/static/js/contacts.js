var tags=[];


function addtask(){
    $('#taskpopover').show();    
}

function removetag()
{
   $(this).parents('li:first').remove();
   var who = $(this).attr('href').replace('#','').replace('tag_','');
   $("[value='"+who+"']").remove();
   $('#'+who).val('');
   $('#search_form').attr('action',location).submit();
}


function tagger()
{
    myvalue = $(this).attr('href').replace('#','');
    if(myvalue!='undefined')
    {
        if(tags.indexOf(myvalue.trim())<0)
            tags.push(myvalue.trim());
    }
    
    $('.tagging').remove();
    $.each(tags,function(x,y){
            var ah=document.createElement('a');
                $(ah).addClass("glyphicon glyphicon-remove movert");
                $(ah).click(function(){
                    tags.splice(tags.indexOf(y),1);
                    $(this).parents('div:first').remove();    
                });
                $(ah).attr('href','#').html('&nbsp;');
                var taginput=document.createElement('input');
                    $(taginput).attr('type','hidden').attr('name','tag');
                    $(taginput).val(y);
                    
                var divbox = document.createElement('div');
                    $(divbox).addClass('tagging').append(taginput);
            $('#buscando').before(divbox);
    });
    $('#search_form').attr('action',location).submit();
}



function rmli()
{
    if($(this).parents('ul:first').find('li').length>1)
        $(this).parents('li:first').remove();
    else
        $(this).parents('li:first').find('input').val('');
    return false;
} 


function addli()
{
        var clonn = $(this).parents('li:first').clone();
        $(clonn).find('input').val('');
        $(clonn).find('.adder').hide();
        $(clonn).find('.deler').removeClass('blind').click(rmli);
        parentme = $(this).parents('ul:first');
        $(parentme).append(clonn);
        return false;
}

function changers(params)
{
    $.each(params,function(x,y){
        $(y.changer).change(function(){
            var valor = $(this).val();
            $(y.victim).html('<option>Espere un momento por favor</option>');
            if(typeof(y.data)=='object'){
                thedata = y.data;
                url = y.url+'?pk='+valor+'&es='+$('#id_estado').val()+'&del='+$('#id_delomun').val();
            }
            else{
                thedata = null;
            
            
            url = y.url+'?'+y.param+'='+valor;
            }
            defaultvalue = '<option value="">------------Seleccione-----------</option>';
            $(y.victim).html(defaultvalue);
            $.getJSON(url,thedata,function(r){
                $.each(r,function(k,v){
                    $(y.victim).append('<option value="'+v.municipio+'">'+v.municipio+'</option>');    
                    
                });
                

            });
        });    
    });
   
}


function remove_err(){
        $(this).siblings('.err:first').remove();   
}

function sendit(e)
{
    e.preventDefault();
    // EXTRACTING DE FORM DATA
    var data = $(this).serializeArray();
    var url = $(this).attr('action');
    var method_is = $(this).attr('method');

    $.ajax({url:url,
            type:method_is,
            data:data,
            dataType:'json',
            success:function(response){
                $('.err').remove();
                $.each(response,function(x,y){
                    if(response.redirect)
                    {
                        window.location.href = '/'+response.redirect;
                    }
                    else
                    {
                        $('#id_'+x).after('<span class=" inline_blocke err">'+y+'</span>').change(remove_err);
                    }
                });
                $('.err:first').siblings().focus();
            }
    });
}

function submitPhoto(){
    if($('#contact_file').val().length>1)
    {
        $('.loadphoto').text('Cargando, por favor espere un momento...').show();
        daform = $(this).parents("form:first");
        //$(dadd).attr('action','/addphoto').attr('target','nopest');
        //$(dadd).submit();
        var data = new FormData($(daform).get(0));
            $.ajax({
                data: data,
                url: '/addphoto',
                type:'post',
                cache: false,
                dataType:'json',
                processData: false,
                contentType: false,
                success:function(resp){
                       cambio = $('.myphoto');
                        idchanger = $("#id_photoc");
                        $(cambio).attr('src',resp.contact_file);
                        $(idchanger).val(resp.pk);
                        $('.delphoto').attr('href','#/delphoto/'+resp.pk+'/');
                        $('.loadphoto').text('Se cargo con exito').fadeOut('100');
                        $("#contact_file").val('');
                         }
                }); 
    }
    else
    {
        alert('Debe seleccionar una imagen');
        $('#contact_file').click();
    }
}




