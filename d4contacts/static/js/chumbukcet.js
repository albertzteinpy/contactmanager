function ajaxcall()
{
    var megran = $(this).parents('.notes_list:first');
    if(confirm('Esta seguro que desea eliminar esta nota'))
    {
        var targetr = $(this).attr('href');
        $.ajax({url:targetr,success:function(response){
            $(megran).remove();
            }
        });
    }
    return false;
} 

function ajax_send_tag()
{
   var data = $(this).serializeArray();
    tagname = $(this).find('[name=tag]').val();
    cid = $('[name=contactpk]').val();
    $.ajax({
            url:'/addtag/',
            data:data,
            type:'post',
            dataType:'json',
            success:function(response){
                $('#tage_boxe').html(' ');
                $.each(response,function(x,y){
                    var divbox = document.createElement('div');
                    var ah = document.createElement('a');
                        $(ah).addClass("glyphicon glyphicon-remove-sign movert");
                        $(ah).attr('id','pice_'+cid);
                        $(ah).click(tagremover);
                        $(ah).attr('href','#'+tagname).html('&nbsp;');
                        $(divbox).append(ah);
                        $(divbox).attr('class','inline_blocke tags').append(y);
                   $('#tage_boxe').append(divbox);
                });
                $('#popover0').hide();
            }
        });
    return false;
}

function edipokill()
{
    $(this).parents('.ligthform:first').remove();
}

function tagshow()
{
    $('.ligthform').remove();
    divbox = document.createElement('div');
    divbox.className='ligthform';
    boxform = $('#tagerbox').clone();
    $(boxform).find('.edipokill').click(edipokill);
    $(boxform).attr('id','').removeClass('blind');
    $(divbox).append(boxform);
    $(this).after(divbox);    
    $(boxform).find('.btn_add_tag').click(ajax_send_tag);
    return false;
}


function tagremover()
{
    var targets = $(this).attr('target'); 
    var tagname = $(this).attr('href').replace('#','');
    var contactid = $(this).attr('id').replace('pice_','');
    var padre = $(this).parents('div:first');
    data = {'tagname':tagname,'cid':contactid,'targets':targets};
    $.ajax({url:'/deltag',
            data:data,
            type:'get',
            success:function(response){
                    $(padre).remove();

                }
            
    });
    return false;
}

function deleteperson()
{
    if(confirm('para eliminar el registro, clic en OK')) return true;
    else return false;
}

function addnote()
{
    var data=$(this).serializeArray();
    var uri=$(this).attr('action');
    var methodman=$(this).attr('method');
    if($('[name=text]').val().length<1){
        alert('Debe escibir una nota');
        return false;
    }
    $.ajax({url:uri,
            data:data,
            type:methodman,
            dataType:'json',
            success:function(response){
                if(response.id)
                {
                    $('[name=text]').val('');
                    var newbox = $('.notes_list:first').clone();
                    $(newbox).removeClass('blind');
                    $(newbox).find('.fecha_s').html(response.formdata.fecha);
                    $(newbox).find('.sender').html(response.formdata.sender);
                    $(newbox).find('.text').html(response.formdata.text);
                    $(newbox).find('[href^="/delnote?id"]').attr('href','/delnote?id='+response.id)
                    .click(ajaxcall);
                    $('.notes_box').prepend(newbox);
                }
                else{
                    alert(response.errors);   
                }
            }});
    return false;
}

function delcontact()
{
        var uris = $(this).attr('href').replace('#','');
        var este = $(this);
        $.getJSON(uris,function(ok){
            alert('En contacto fué retirado de esta Asociación.');
            $(este).parents('li:first').remove();
        });    
}

function editor_open()
{
    dadd = $(this).parents('.notes_list:first');    
    $(dadd).find('.txt_mode').addClass('blind');
    $(dadd).find('.edit_mode').removeClass('blind');
}

$(document).ready(function(){
    $('.addtag').click(tagshow);
    $('.deleteperson').click(deleteperson);
    $('#addnotes').submit(addnote);
    $('.removetagg').click(tagremover);
    $('#relcontacts').submit(function(){
       var data = $(this).serializeArray();
       $.ajax({url:'/relcontacts',
               data:data,
               type:'get',
               dataType:'json',
               success:function(response){
                   //location.reload(); 
                   $.each(response,function(x,y){
                        if($('#c_'+y.pk).length<1)
                        {
                            var cloning = $('#list_results li:first').clone();
                            $(cloning).attr('id','c_'+y.pk).removeClass('blind');
                            $(cloning).find('.namec').html(y.namec);
                            $(cloning).find('.email_listing:first').html('<p>'+y.email+'</p>');
                            $(cloning).find('.delcontact').attr('href','#/removethis/'+y.pk+'/'+y.ent+'');
                            remover = $(cloning).find('.delcontact');
                            $(remover).click(delcontact);
                            if(y.status=0)
                                url_c = '/edtcontact/'+y.pk
                            else
                                url_c = '/detailcontact/'+y.pk
                            $(cloning).find('.linking').attr('href',url_c);
                            $(cloning).find('.myminiphoto').css('background-image','url('+y.photox+')');
                            $(cloning).find('.addr').html('');
                            $('#list_results').prepend(cloning);
                        }
                   });
                    forma = document.getElementById('relcontacts');
                    forma.reset();
               }
       })
       return false;
    });
    $('.delcontact').click(delcontact);
    $('#chowyourform').click(function(){
        $('#form_box_relcontact').removeClass('blind');    
    });
});
