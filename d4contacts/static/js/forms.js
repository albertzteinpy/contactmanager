var emails = [];
function cloning()
{
    var parentCloner = $(this).parents("li:first");
    clone = $(parentCloner).clone();
    $(clone).find(".adder").click(cloning);
    $(clone).find(".deler").click(deler);
    $(clone).find('input').val('');
    $(clone).find('.void,.v_email').focus(clean_the_house);
    $(clone).find('.ui-selectmenu-button').remove();
    $(clone).find('.selector').attr('id','');
    $(clone).find('.ui-selectmenu-button').css('width','65px');
    $(clone).find('.v_contacto').autocomplete({source:emails});
    $(parentCloner).after(clone);
}


function cloning_contact()
{
    var parent_clone = $(this).attr("role");
    var sonclon = $(this).attr("data");
    var parentCloner = $("."+sonclon+":last");
    clone = $(parentCloner).clone();
    $(clone).find(".adder").click(cloning);
    $(clone).find(".adderc").click(cloning_contact);
    $(clone).find(".deler").click(deler);
    $(clone).find('input').val('').autocomplete({source:emails});
    $(clone).find('.void,.v_email').focus(clean_the_house);
    $(clone).find('.ui-selectmenu-button').remove();
    $(clone).find('.selector').attr('id','');
    $(clone).find('.ui-selectmenu-button').css('width','65px');
    $(clone).find('.v_contacto').autocomplete({source:emails});
    $(parentCloner).after(clone);
}




function deler()
{
    var parentToDel = $(this).parents("li:first");
    items = $(this).attr("href").replace('#remover','');
    if($("[name="+items+"]").length>1)
        $(parentToDel).remove();
    else
        $(parentToDel).find("input").val('');
}


function submitPhoto(){
    if($('#contact_file').val().length>1)
    {
        $('.loadphoto').text('Cargando, por favor espere un momento...').show();
        dadd = $(this).parents("form:first");
        $(dadd).attr('action','/addphoto').attr('target','nopest');
        $(dadd).submit();
        return false;
    }
    else
    {
        alert('Debe seleccionar una imagen');
        $('#contact_file').click();
        return false;
    }
}

function submitEPhoto(){
    if($('#entity_file').val().length>1)
    {
        $('.loadphoto').text('Cargando, por favor espere un momento...').show();
        dadd = $(this).parents("form:first");
        $(dadd).attr('action','/addphotoentity').attr('target','nopest');
        $(dadd).submit();
        return false;
    }
    else
    {
        alert('Debe seleccionar una imagen');
        $('#entity_file').click();
        return false;
    }
}


function delPhoto(){
    var victim = $(this).attr('href').replace('#','');
    
    if(victim!='/delphoto//' && confirm('¿Esta seguro que desea eliminar la foto?'))
    {
        $.ajax({url:victim,dataType:'json',success:function(response){
                $('.myphoto').attr('src','/static/images/nophoto.jpg');
                $('#photoc').val('');
            }
        });    
    }
    return false;
    
}

var err_validation = {};

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
};

function validator()
{
    $('.err').remove();
    $(".void").each(function(){
            if($(this).val().length<=0)
            {
                $(this).after('<span class="inline_blocke err">debe contener algo de información.</span>');
            }

    });
    $('.v_email').each(function(){
            if(!isValidEmailAddress($(this).val()))
            {
                $(this).after('<span class="inline_blocke err">El dato email no es correcto.</span>');
            }
        
    });

    return $('.err').length;
}

function clean_the_house(){
    vno = $(this).parents('li:first').find('.err');    
    $(vno).remove();
}

function sendPost()
{
        var urli = $(this).attr('action');
        var daform = $(this);
        if($(this).attr("action")=='/addphoto' || $(this).attr("action")=='/addphotoentity'){
        //event.preventDefault();
            var data = new FormData($(daform).get(0));
            $.ajax({
                data: data,
                url: urli,
                type:'post',
                cache: false,
                dataType:'json',
                processData: false,
                contentType: false,
                success:function(resp){
                    if(urli=='/addphoto')
                    {
                        cambio = $('.myphoto');
                        idchanger = $("#photoc");
                        idchanger2 = $("#photoa");
                        $(cambio).attr('src',resp.contact_file);
                        $(idchanger).val(resp.pk);
                        $(idchanger2).val(resp.pk);
                        $('.delphoto').attr('href','#/delphoto/'+resp.pk+'/');
                        $('.loadphoto').text('Se cargo con exito').fadeOut('100');
                        $("#contact_file").val('');
                    }
                    if(urli=='/addphotoentity')
                    {
                        cambio = $('.myphoto');
                        idchanger = $("#photoc");
                        idchanger2 = $("#photoa");
                        $(cambio).attr('src',resp.entity_file);
                        $(idchanger).val(resp.pk);
                        $(idchanger2).val(resp.pk);
                        $('.delphoto').attr('href','#/delphoto/'+resp.pk+'/');
                        $('.loadphoto').text('Se cargo con exito').fadeOut('100');
                        $('#entity_file').val('');
                    }
                }
                }); 
            return false;
            //return true;
        }
        else{
            var validated = validator();   
            var actionsef = $(this).attr('action');
            if(validated==0)
            {
                data = $(this).serializeArray();
                $.ajax({url:'/'+actionsef,
                    data:data,
                    type:'post',
                    dataType:'json',
                    success:function(response){
                        
                        if(actionsef=='addentity')
                            details = '/detailentity/';
                        else
                            details = '/detailcontact/';
                        
                        if(response.saved==true){
                            if(modal===false)
                            {
                                window.location=details+response.pk+'/';
                            }
                            else
                            {
                               $('#addPersonal').modal('hide');
                               $('#addPersonal .modal-body').html('Cargando formulario...');
                            }
                        }
                        else{
                            $.each(response.err,function(x,y){
                                $('#'+x).after('<span class="inline_blocke err">'+y+'</span>');
                            });    
                            
                        }


                    }
                });
            }
        return false;
        }
    
}


function srchandshow()
{
    myvalue = $(this).val();
    myvalidated = isValidEmailAddress(myvalue);
    if(myvalidated)
    {
        $.ajax({url:'check/'+myvalue,dataType:'json',success:function(response){
            }
        
        });
    }
}


function restColonias()
{
    data = {'pk':$(this).val()};
    $.ajax({url:'/colonia',
            type:'get',
            dataType:'json',
            data:data,
            success:function(response){
                var whereis = $('#tocolonia');
                datas = response;
                parseLocations(whereis,datas,'colonia');
            }
    });

}


function parseLocations(whereis,datas,nameson)
{
    $('#'+nameson).remove();
    var selector = document.createElement('select');
    selector.name = nameson;
    selector.id = nameson;
    $(selector).addClass('form-control').append('<option value="">Seleccione</option>');
    $.each(datas,function(x,y){
        $(selector).append('<option value="'+y.municipio+'">'+y.municipio+'</option>');    
    });
    $(whereis).after(selector);
    if(nameson=='delomun' && location.pathname.indexOf('edtcontact')=='-1')
    $(selector).change(restColonias);
    if(nameson=='delomun' && location.pathname.indexOf('edtcontact')>'-1'){
        $(selector).val($('#otherdelomun').val());    
        $(selector).change(restColonias);
    }
    if(nameson=='colonia' && location.pathname.indexOf('edtcontact')>'-1')
    {
        $(selector).val($('#othercolonia').val());
        
    }

    if(nameson=='colonia' && location.pathname.indexOf('edtcontact'=='-1'))
    {
        if(modal===false)
        $(selector).change(localizeme);
        
    } 
}


function restLocation()
{
    data  = {'pk':$(this).val()};
    $.ajax({url:'/location',
             type:'get',
            dataType:'json',
            data:data,
            success:function(response){
                 var whereis = $('#tomunicipio');
                datas = response;
                parseLocations(whereis,datas,'delomun');
            }
    });

}





function chowinput()
{
   var victim = $(this).attr('href');
   $(victim).show();

}



$(document).ready(function(){
    $(".adder").click(cloning);    
    $(".adderc").click(cloning_contact);  
    $(".deler").click(deler);
    $('.void,.v_email').focus(clean_the_house);
    $("#addcontact,#addentity").submit(sendPost);
    $("#addphoto").click(function(){
        $("#contact_file").click();    
    });
    $("#addephoto").click(submitEPhoto);
    $('.delphoto').click(delPhoto);
    $("#btn_save").click(function(){
        $(this).parents("form:first").attr('action',$(this).parents("form:first").attr('id'));    
    });
    $('.v_contactoa').keyup(srchandshow);
    //$('.v_phone').keyme();
    $("#contact_file").change(submitPhoto);
    $("#entity_file").change(submitEPhoto);
   if(location.pathname.indexOf('edtcontact')=='-1')
   {   
        $('.location_dropu').selectmenu({
            change:restLocation
        });

        $('#estado,#estadoa').change(restLocation);
    }

    
   // TO SEARCH PANEL ----------------------------------------------------------------------------

    $('.chowinput').click(function(){
        $('#search_form').attr('action',location);
        var victim = $(this).attr('href');
        var whereami = $(this).position();
        var topp = $(this).height()+whereami.top+15;
        $('.tab_s').addClass('blind');
        $('#boxy_float').css('left',whereami.left+'px').css('top',topp+'px').removeClass('blind');
        $(victim).removeClass('blind');
        $('#buscando').removeClass('blind');
        $('#boxy_float').removeClass('blind').show();
        $('.apaca').show();
        return false;
    });


    $('.locatio_drop,#estadoa').change(restLocation);
    
    $('.mover').click(function(){
         var victim = $(this).attr('href');
        $(victim).find('input:first').val('');
        $(victim).addClass('blind');
    });
    $('.movert').click(function(){
        $(this).parents('div:first').remove();    
    });

    $('.v_contacto').autocomplete({
        source:emails    
    });

    $('.closer').click(function(){
        var who_close = $(this).attr('href');
        $(who_close).addClass('blind');
    });
});
