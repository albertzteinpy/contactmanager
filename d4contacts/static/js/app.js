$( document ).ready(function() {
    $('.popover-dismiss').popover({
	  trigger: 'focus'
	})

	$( "#id_Datehechos" ).datepicker({
     	dateFormat: 'dd/mm/yy',
		yearRange: "1920:2014",
		dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Jue', 'Vie', 'S&aacute;b'],
		changeMonth: true,
		changeYear: true,
	});
	$( "#id_Datelegal" ).datepicker({
		dateFormat: 'dd/mm/yy',
			yearRange: "1920:2014",
			dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Jue', 'Vie', 'S&aacute;b'],
			changeMonth: true,
			changeYear: true,
	});
	$( "#id_Dategire" ).datepicker({
		dateFormat: 'dd/mm/yy',
			yearRange: "1920:2014",
			dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Jue', 'Vie', 'S&aacute;b'],
			changeMonth: true,
			changeYear: true,
	});
	$( "#fechaCierre" ).datepicker({
		dateFormat: 'dd/mm/yy',
			yearRange: "1920:2014",
			dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Jue', 'Vie', 'S&aacute;b'],
			changeMonth: true,
			changeYear: true,
	});
	$( "#fechaPosible" ).datepicker({
		dateFormat: 'dd/mm/yy',
			yearRange: "1920:2014",
			dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Jue', 'Vie', 'S&aacute;b'],
			changeMonth: true,
			changeYear: true,
	});
	 $('.descripcion').readmore({
	 	moreLink: '<a href="#">Leer Más</a>',
	 	lessLink: '<a href="#">Cerrar</a>',
	 	maxHeight: 40
	});
	$('#id_Timehechos').datetimepicker({
		datepicker:false,
		format:'H:i',
		step:10
	});
	$('#timepicker1').datetimepicker({
		datepicker:false,
		format:'H:i',
		step:10
	});
	$('#div_id_redes').hide();
	$('#div_id_dirweb').hide();
	$(document).on('change','#id_tipofuente', selectFuente)
});

function selectFuente(){
	console.log($( "#id_tipofuente option:selected" ).text());
	var fuente = $( "#id_tipofuente option:selected" ).text();
	if (fuente == "Radar") {
		$('#div_id_dirweb').show();
		$('#div_id_name').hide();
	}else if (fuente == "Organización") {
		$('#div_id_name').show();
		$('#div_id_redes').show();
		$('#div_id_dirweb').show();
	}else if(fuente == 'Redes Sociales'){
		$('#div_id_redes').show();
		$('#div_id_dirweb').show();
	}else if (fuente == 'Nota periodística') {
		$('#div_id_dirweb').show();
	}else{
		$('#div_id_redes').hide();
		$('#div_id_dirweb').hide();
	};
}