from django.contrib import admin
from appcontact.models import * 


'''
class ExtraFieldAdmin():
    inlines = [OptionFields,]
    model = ExtraFields
admin.site.register(ExtraFields,ExtraFieldAdmin)
#admin.site.register(Contact)
'''

class OptionsFieldAdmin(admin.StackedInline):
    model = OptionFields 

class ExtraFieldx(admin.ModelAdmin):
    inlines = [OptionsFieldAdmin,]

admin.site.register(ExtraFields,ExtraFieldx)
