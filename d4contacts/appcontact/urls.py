from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import *

urlpatterns = patterns('appcontact.views',
    url(r'^$','index',name='index'),
    url(r'^contacto$','contact_form',name='contact_form'),
    url(r'^inicio$','inicio',name='inicio'),
    url(r'^logout$','logoff',name='logoff'),
    url(r'^mycontacts','mycontacts',name='mycontacts'),
    url(r'^detailcontact/(?P<idc>\d+)/$','detailcontact',name='detailcontact'),
    url(r'^edtcontact/(?P<idc>\d+)/$','edtcontact',name='edtcontact'),
    url(r'^delcontact/(?P<idc>\d+)/$','delcontact',name='delcontact'),
    url(r'^resultados$','resultados',name='resultados'),

)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


urlpatterns += patterns('appcontact.restviews',
    url(r'^login$','login',name='login'),
    url(r'^addcontact$','addcontact',name='addcontact'),
    url(r'^addphoto$','addphoto',name='addphoto'),
    url(r'^delphoto/(?P<idu>\d+)/$','delphoto',name='delphoto'),
    url(r'^location$','restlocation',name='restlocation'),
    url(r'^addtag/$','addtag',name='addtag'),
    url(r'^delnote$','delnote',name='delnote'),
    url(r'^colonia$','colonia',name='colonia'),
    url(r'^addnote$','addnote',name='addnote'),
    url(r'^deltag$','deltag',name='deltag'),
    url(r'^cps$','cps',name='cps'),
    
)
