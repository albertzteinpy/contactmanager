# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, permission_required
import simplejson
from django.contrib.auth import authenticate,login as auth_login,logout
from contactmanager.forms import *
from appcontact.models import *
from django.conf import settings
import datetime

def tagging(tag,entity,pk):
    tag = tag.strip()
    if tag:
        tagbd,failtag = Tag.objects.get_or_create(tagname=tag)
        if entity:
            c = Entity.objects.get(pk=pk)
        else:
            c = Contact.objects.get(pk=pk)
    
        if c.tags:
            current_tag = simplejson.loads(c.tags) 
        else:
            current_tag = []
        current_tag.append(tagbd.tagname)
        current_tag = sorted(set(current_tag))
        c.tags = simplejson.dumps(current_tag)
        c.save()
        texttags = Tag.objects.filter(tagname__in=current_tag).values('tagname')
        texttags = [x['tagname'] for x in texttags]
        tag = simplejson.dumps(texttags)
        return tag
    else:
        return simplejson.dumps({'notag':'No es tag valido'})


def login(request):
    usr = request.POST.get("usr")
    pwd = request.POST.get("pwd")
    user = authenticate(username=usr, password=pwd)
    if user:
        if user.is_active:
            auth_login(request,user)
            response = {'l':'ok'}
    else:
        response = {'l':'fail'}
    
    return HttpResponse(simplejson.dumps(response))

@login_required(login_url="/")
def checkcontact(request,contacto):
        emailcheck= EmailList.objects.filter(email=contacto)
        if not emailcheck:
            #c = Contact(user=request.user,namec='Untitle')
            #c.save()
            #emailcheck = EmailList(contacto=c,email=contacto)
            #emailcheck.save()
            response={'r':'newc'}
        else:
            response={'r':'checked'}

        return HttpResponse(simplejson.dumps(response))


def addingMeta(params=None):
    if params:
        for p in params:
            m,fail = MetaContact.objects.get_or_create(
                                                asignetstring=p['strings'],
                                                asignetto=p['asignedto'],
                                                metastring=p['metastring'],
                                                metavalue = p['metavalue'],
                                                contact_meta = p['contact']
                     
                        )
        return True
    else:
        return False



@login_required(login_url="/")
def addcontact(request):
    idc = request.POST.get('id',None)
    if idc:
        instance = Contact.objects.get(pk=idc)
        m = MetaContact.objects.filter(contact_meta=instance)
        iemailis = instance.emaillist_set.all()
        iemailis.delete()
        m.delete()
    else:
        instance = None

    data = request.POST.copy()
    modelo = Contact
    modelform = FormCreator()
    modelform = modelform.form_to_model(modelo)
    modelform = modelform(data,instance=instance)
    extras = ExtraFields.objects.all().order_by('ordering') 
    extraform = DynamicForm()
    extraform = extraform.from_model(extras)
    tags = request.POST.getlist('tags')
    redsocial = request.POST.getlist('redsocial')
    redsociallist = request.POST.getlist('redsociallist')
    direccion =  request.POST.getlist('direccion')
    wdir = request.POST.getlist('whereisdir')
    data['usercreator'] = request.user.pk
    data['status']='1'
    data['tags']=None
    #data['date_create']=datetime.datetime.now()
    errors = {}
    success = {}
    m = None

    if modelform.is_valid():
        m = modelform.save()
        success.update({'modelform':'ok','pk':m.pk})
        for t in tags:
            tagging(t,None,m.pk)
        if extraform:
            extraform = extraform(data)
            if extraform.is_valid() and m:
                pices = [ {'strings':'contact',
                       'asignedto':x.name.replace('q_',''),
                       'metastring':u'%s'%x.label,
                       'metavalue':u'%s'%x.value(),
                       'contact':m } for x in extraform ]
                metas = addingMeta(pices)
                success.update({'extra':'ok'})
            else:
                errors.update(extraform.errors)

        contactlist = request.POST.getlist('contactinfo')
        typeof = request.POST.getlist('typeof')
        whereis = request.POST.getlist('whereis')
        piceofsh = []
        
        for singlec in contactlist:
            if singlec:
                if typeof[contactlist.index(singlec)]=='Email':
                    regemail,failer = EmailList.objects.get_or_create(contacto=m,email='%s'%singlec)
                    if regemail:
                        regemail.typeofemail=u'%s'%typeof[contactlist.index(singlec)]
                        regemail.save()
                        pices_contact = None 
                else:
                    singlec = singlec
                    pices_contact = [{'strings':'contact',
                                  'asignedto':100,
                                  'metastring':u'%s'%typeof[contactlist.index(singlec)],
                                  'metavalue':u'%s|%s'%(singlec,whereis[contactlist.index(singlec)]),
                                  'contact':m }] 
                    piceofsh.append(pices_contact)
            else:
                pices_contact = None
            
            if pices_contact:
                metas = addingMeta(pices_contact)

    else:
        errors.update(modelform.errors)
    rss = []
    for rs in redsocial:
        if rs:
            rsdict = [{'strings':'redsocial',
                       'asignedto':1,
                       'metastring':u'%s'%redsociallist[redsocial.index(rs)],
                       'metavalue':u'%s'%rs,
                       'contact':m
                    }]
            metas = addingMeta(rsdict)

    for d in direccion:
        if d:
            rsdict = [{'strings':'direccion',
                       'asignedto':1,
                       'metastring':u'%s'%wdir[direccion.index(d)],
                       'metavalue':u'%s'%d,
                       'contact':m
                    }]
            metas = addingMeta(rsdict)
       
  
    if errors:
        response = simplejson.dumps(errors)
    else:
        success['redirect']='mycontacts'
        response = simplejson.dumps(success)
    return HttpResponse(response)


@login_required(login_url="/")
def addphoto(request):
    if request.POST:
        context = RequestContext(request)
        fields = ['contact_file','type_file']
        response = {}
        forma = FormCreator()
        modelo = ContactFiles
        data = request.POST.copy()
        data['type_file']='profilePhoto'
        files = request.FILES
        response['contact_file']='/static/%sContactFiles/%s' %(settings.MEDIA_URL,str(files['contact_file']))
        formaldeido = forma.advanced_form_to_model(modelo,fields)
        formaldeido = formaldeido(data,request.FILES)
        if formaldeido.is_valid():
            newphotoc = ContactFiles(contact_file=request.FILES['contact_file'],type_file=data['type_file'])
            newphotoc.save()
            response['pk']=newphotoc.pk
            return HttpResponse(simplejson.dumps(response))
            #return render_to_response('pices/uploaded.html',{'data':response},context)
        else:
            return HttpResponse(simplejson.dumps(formaldeido.errors))
    else:
        return HttpResponse()


def delphoto(request,idu):
    context = RequestContext(request)
    if idu:
        try: p = ContactFiles.objects.get(pk=idu).delete()
        except: p = None
    response = {'del':True}
    
    return HttpResponse(simplejson.dumps(response))

@login_required(login_url="/")
def addtag(request):
    tag = request.POST.get('tag',None)
    pk = request.POST.get('contactpk',None)
    entity = request.POST.get('typo',None)
    if pk:
        tag = tagging(tag,entity,pk)
    else:
        tag = 'FAIL'
    return HttpResponse(tag) 


@login_required(login_url='/')
def deltag(request):
    context = RequestContext(request)
    contacto = request.GET.get('cid',None)
    tagname = request.GET.get('tagname',None)
    targets = request.GET.get('targets',None)
    if contacto:
        if targets:
            c = Entity.objects.get(pk=contacto)
        else:
            c = Contact.objects.get(pk=contacto)

        ctags = simplejson.loads(c.tags)
        try:
            idtag = ctags.index(tagname)
            del ctags[idtag]
        except:
            pass
        newtags = simplejson.dumps(ctags)
        c.tags=newtags
        c.save()
        
    return HttpResponse('El Tag se eliminó correctamente.')



@login_required(login_url="/")
def myentities(request):
    return HttpResponse('') 


def restlocation(request):
    pk = request.GET.get('pk',None)
    if pk:
        municipio = Sepomex.objects.filter(estado=pk).distinct('municipio').values('clave_municipio','municipio').order_by('municipio')
        municipio = [{'pk':x['clave_municipio'],'municipio':x['municipio']} for x in municipio]
        municipio = simplejson.dumps(municipio)
    else:
        municipio = None
    return HttpResponse(municipio)


def colonia(request):
    pk = request.GET.get('pk',None)
    if pk:
        municipio = Sepomex.objects.filter(municipio=pk).distinct('asentamiento').values('clave_asentamiento','asentamiento').order_by('asentamiento')
        municipio = [{'pk':x['clave_asentamiento'],'municipio':x['asentamiento']} for x in municipio]
        municipio = simplejson.dumps(municipio)
    else:
        municipio = None
    return HttpResponse(municipio)

def cps(request):
    pk = request.GET.get('pk',None)
    estado= request.GET.get('es',None)
    mun = request.GET.get('del',None)
    if pk:
        cpsx = Sepomex.objects.filter(asentamiento__icontains=u'%s'%pk,estado__icontains=estado,municipio__icontains=mun).distinct('codigo_postal').values('codigo_postal').order_by('codigo_postal')
     
        cps = [{'pk':x['codigo_postal'],'municipio':x['codigo_postal']} for x in cpsx]
        cps = simplejson.dumps(cps)
    else:
        cps = None
    return HttpResponse(cps)

@login_required(login_url="/")
def addnote(request):
    form = FormCreator()
    modelo = Note
    data = request.POST.copy()
    formaldeido = form.form_to_model(modelo)
    formaldeido= formaldeido(data)
    if formaldeido.is_valid():
        doit = formaldeido.save()
        try: sendername = Contact.objects.get(userrelated=doit.sender.pk)
        except: sendername = None
        if sendername:
            formdata = {'receptor':'%s %s' %(doit.receptor.namec,doit.receptor.lastname),
                        'sender':'%s %s' %(sendername.namec,sendername.lastname),
                        'text':doit.text,
                        'fecha':'%s' %(doit.date_create)
                        }
        else:
            formdata = {'receptor':'%s %s' %(doit.receptor.namec,doit.receptor.lastname),
                        'sender':'',
                        'text':doit.text,
                        'fecha':'%s' %(doit.date_create)
                        }

        

        response = simplejson.dumps({'id':doit.pk,'formdata':formdata})
        return HttpResponse(response)
    else:
        errors = simplejson.dumps(formaldeido.errors)
        return HttpResponse(errors)


@login_required(login_url="/")
def delnote(request):
    idnote = request.GET.get('id',None)
    response = {}
    if idnote:
        n = Note.objects.get(pk=idnote)
        n.delete()
        response['del']='ok'
    else:
        response['del']='fail'
    

    return HttpResponse(response)
