# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
import simplejson
from tasks.models import *

class ContactFiles(models.Model):
    type_file = models.CharField(max_length=200,blank=True)
    contact_file = models.FileField(upload_to='ContactFiles')
    '''
        status params 
         case 0 incomplete
         case 1 Complete
         case 2 Deleted
    '''
class Contact(models.Model):
    userrelated = models.ForeignKey(User,null=True,blank=True)
    usercreator = models.IntegerField(null=True,blank=True)
    namec = models.CharField(u'Nombre(s) ',max_length=200)
    lastname = models.CharField(u'Apellido(s)',max_length=200,blank=True,null=True)
    chargec = models.CharField(u'Cargo',max_length=100,blank=True,null=True)
    date_create = models.DateTimeField(default=datetime.now,auto_now_add=True)
    status = models.CharField(max_length=2,default=1)
    estado = models.CharField(u'Estado',max_length=200)
    delomun = models.CharField(u'Del./Municipio',max_length=200)
    colonia = models.CharField(u'Colonia',max_length=200)
    cp = models.CharField(u'C.P.',max_length=10,blank=True,null=True,default=100)
    address = models.CharField(u'Dirección',max_length=200,blank=True,null=True,default=' ')
    lon = models.FloatField(default=0)
    lat = models.FloatField(default=0)
    extrac = models.TextField(blank=True,null=True)
    photoc = models.IntegerField(blank=True,null=True)
    tags = models.TextField(blank=True,null=True)

    def photoimg(self):
        try: 
            photoim = ContactFiles.objects.get(pk=self.photoc)
        except: 
            photoim = None
        return photoim

    def tages(self):
        if self.tags:
            tags = simplejson.loads(self.tags)
            listing = tags
        else:
            listing = None
        return listing

    def mytasks(self):
        t = None #Tasking.objects.filter(asigned_string='contact',asignedto=self.pk)
        return t
    
    def __unicode__(self):
        return u'%s'%self.namec

    def social(self):
        return self.metacontact_set.filter(asignetstring='redsocial')

    def contacto(self):
        tels = self.metacontact_set.filter(asignetstring='contact',metastring__icontains='Tel')
        fones = []
        for t in tels:
            metavalue = t.metavalue.replace('|',' ')
            fones.append({'metastring':t.metastring,'metavalue':metavalue})
        return fones

    def addresses(self):
        return self.metacontact_set.filter(asignetstring='direccion')


class MetaContact(models.Model):
    asignetstring = models.CharField(max_length=200)
    asignetto = models.IntegerField()
    metastring = models.CharField(max_length=200)
    metavalue = models.CharField(max_length=200)
    contact_meta = models.ForeignKey(Contact)




class EmailList(models.Model):
    contacto = models.ForeignKey(Contact)
    email = models.EmailField(unique=True)
    typeofemail = models.CharField(max_length=10,blank=True)


class Tag(models.Model):
    tagname = models.CharField(max_length=200,null=True,default='Untitle')
    def __unicode__(self):
        if self.tagname:
            return '%s' %(self.tagname[0].upper())
    
class Sepomex(models.Model):
    codigo_postal = models.CharField(max_length=5)
    asentamiento = models.CharField(max_length=250)
    tipo_asentamiento = models.CharField(max_length=100)
    municipio = models.CharField(max_length=250)
    estado = models.CharField(max_length=50)
    ciudad = models.CharField(max_length=250)
    descripcion_codigo_postal = models.CharField(max_length=5)
    clave_estado = models.CharField(max_length=2)
    clave_oficina = models.CharField(max_length=5)
    clave_tipo_asentamiento = models.CharField(max_length=5)
    clave_municipio = models.CharField(max_length=3)
    clave_asentamiento = models.CharField(max_length=2)
    zona = models.CharField(max_length=40)
    clave_ciudad = models.CharField(max_length=2)

class Note(models.Model):
    class Meta:
        ordering = ['date_create']
    
    text = models.TextField(default='untitle')
    sender = models.ForeignKey(User)
    receptor = models.ForeignKey(Contact)
    date_create = models.DateField(auto_now_add=True,default=datetime.now)


TYPES = (('option','option'),
         ('open','text'),
        )
class ExtraFields(models.Model):
    fieldname = models.CharField(u'Nombre del campo',max_length=200)
    typefield = models.CharField(u'Tipo de campo',max_length=200,choices=TYPES)
    ordering = models.IntegerField(default=1)
    
    def __unicode__(self):
        return '%s'%self.fieldname

class OptionFields(models.Model):
    fieldp = models.ForeignKey(ExtraFields)
    optiontxt = models.CharField(u'Opción',max_length=200)

    def __unicode__(self):
        return u'%s'%self.optiontxt

