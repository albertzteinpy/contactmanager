# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from contactmanager.forms import *
from appentity.models import * 
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
from django.contrib.auth import logout
import string as STrg
from appcontact.views import abc_tag_list 
from django.db.models import Q


ESTADOS = [('','------------Seleccione-----------'),]
ESTADOS += [(x.estado,x.estado) for x in Sepomex.objects.all().distinct('estado')]

@login_required(login_url='/')
def entity(request):
    if request.is_ajax():
        INDEX_EXTEND = 'blank.html'
    else:
        INDEX_EXTEND = 'base2.html'
    context = RequestContext(request)
    states=None
    modelo = Entity
    forma = FormCreator()
    excludes = ('usercreator','userrelated','status','date_create','phones','tags')
    widgets = {
                'estadoa':forms.Select(choices=ESTADOS,attrs={'class':'sensao'}),
                'delomun':forms.Select(),
                'colonia':forms.Select(),
                'cpac':forms.Select(),
                'latac':forms.HiddenInput(),
                'lonac':forms.HiddenInput(),
                'photoa':forms.HiddenInput()
              }
    forma = forma.advanced_form_to_model(modelo=modelo,excludes=excludes,widgets=widgets)
    extras = ExtraFields.objects.all().order_by('ordering') 
    extraform = DynamicForm()
    extraform = extraform.from_model(extras)
    contactform=rsform=addsform = [Formcontact()]
    return render_to_response('forms/eyform.html',
                              {'lista':'forms_list',
                              'extraform':extraform,
                              'forma':forma,
                              'cform':contactform,
                              'rsform':rsform,
                              'addsform':addsform,
                              'states':states,'extend':INDEX_EXTEND},
                              context)




@login_required(login_url='/')
def myentities(request):
    context = RequestContext(request)
    args = Q()
    kwargs = {}
    nombre = request.POST.get('namec',None)
    tags = request.POST.getlist('tag')
    estado = request.POST.get('estado',None)
    delomun = request.POST.get('delomun',None)
    emailstr = request.POST.get('email',None)
    if nombre:
        args.add(Q(nameac__icontains=nombre),Q.AND)
    if tags:
        for t in tags:
            args.add(Q(tags__icontains='%s'%(t)),Q.AND)
    if estado and not delomun:
            args.add(Q(estadoac__icontains='%s'%(estado)),Q.AND)
    if delomun: 
            args.add(Q(delomun__icontains='%s'%(delomun)),Q.AND)
    if emailstr:
            ilista = EntityEmailList.objects.filter(email__icontains='%s'%(emailstr)).values('entity_id')
            ids = [(x['entity_id']) for x in ilista]
            args.add(Q(id__in=ids),Q.AND)
    entities = Entity.objects.filter(*[args],**kwargs)
    try: 
        myme = Contact.objects.get(userrelated=request.user.pk)
    except:
        myme = request.user
    abc = abc_tag_list()
    formdata = request.POST.copy()
    try: del formdata['csrfmiddlewaretoken']
    except: pass
    try: del formdata['tag']
    except: pass
    return render_to_response('myentities.html',{'entities':entities,
                                                 'abc':abc,
                                                 'myme':myme,
                                                 'formdata':formdata,
                                                 'tags':tags,
                                                 },context)

@login_required(login_url='/')
def detailentity(request,ide=None):
    context = RequestContext(request)
    try:
        ent = Entity.objects.get(pk=ide)
    except:
        ent = None
    try: 
        myme = Contact.objects.get(userrelated=request.user.pk)
    except:
        myme = request.user
    emails = EmailList.objects.all().distinct('email')
    return render_to_response('detailentity.html',{'ent':ent,'myme':myme,'emails':emails},context)




@login_required(login_url='/')
def editentity(request,ide=None):
    if ide:
        c = Entity.objects.get(pk=ide)
    else:
        return HttpResponse('You need an ID')

    if request.is_ajax():
        INDEX_EXTEND = 'blank.html'
    else:
        INDEX_EXTEND = 'base2.html'

    context = RequestContext(request)
    states=None
    modelo = Entity
    forma = FormCreator()
    excludes = None#('usercreator','userrelated','status','date_create','phones','tags')
    municipios = Sepomex.objects.filter(estado=c.estadoa).distinct('municipio').values('municipio')
    colonias = Sepomex.objects.filter(municipio=c.delomun).distinct('asentamiento').values('asentamiento')
    cps = Sepomex.objects.filter(asentamiento=c.colonia,
                                 municipio=c.delomun,
                                 estado=c.estadoa).distinct('codigo_postal').values('codigo_postal')
    
    MUNICIPIO = [('','----seleccione-----------')]
    COLONIA = [('','----seleccione-----------')]
    CP = [('','----seleccione-----------')]
    MUNICIPIO += [(x['municipio'],x['municipio']) for x in municipios]
    COLONIA += [(x['asentamiento'],x['asentamiento']) for x in colonias]
    CP += [(xc['codigo_postal'],xc['codigo_postal']) for xc in cps]
    print CP
    widgets = {
                'estadoa':forms.Select(choices=ESTADOS,attrs={'class':'sensao'}),
                'delomun':forms.Select(choices=MUNICIPIO),
                'colonia':forms.Select(choices=COLONIA),
                'cpac':forms.Select(choices=CP),
                'latac':forms.HiddenInput(),
                'lonac':forms.HiddenInput(),
                'photoa':forms.HiddenInput()
              }
    forma = forma.advanced_form_to_model(modelo=modelo,excludes=excludes,widgets=widgets)
    forma = forma(instance = c)
    extras = ExtraFields.objects.all().order_by('ordering') 
    extraform = DynamicForm()
    extraform = extraform.from_model(extras)
    initializing = MetaEntity.objects.filter(contact_meta=ide)
    
    contactform = []
    addsform = []
    rsform = []
    for ini in initializing:
        if ini.metavalue:
            if ini.asignetstring in 'entity':
                metavalues = ini.metavalue.split('|')
                try: 
                    contactinfo = metavalues[0]
                    whereis = metavalues[1]
                except: 
                    contactinfo = None
                    whereis = None
                initial = {'typeof':ini.metastring,'contactinfo':contactinfo,'whereis':whereis}
                contactform += [Formcontact(initial=initial)] 
            if ini.asignetstring in 'redsocial':
                initial = {'redsociallist':ini.metastring,'redsocial':u'%s'%ini.metavalue}
                rsform += [Formcontact(initial=initial)] 
            if ini.asignetstring in 'direccion':
                initial = {'whereisdir':ini.metastring,'direccion':u'%s'%ini.metavalue}
                addsform += [Formcontact(initial=initial)] 
    
    fd = [Formcontact()]
    if len(addsform)==0:
         addsform = fd
    if len(contactform)==0:
            contactform = fd
    if len(rsform)==0:
            rsform = fd

    photo = EntityFile.objects.filter(pk=c.photoa)
    if photo:
        photo = photo[0].entity_file
    return render_to_response('forms/eyform.html',
                              {'lista':'forms_list',
                              'extraform':extraform,
                              'forma':forma,
                              'cform':contactform,
                              'rsform':rsform,
                              'addsform':addsform,
                              'ide':ide,
                              'photo':photo,
                              'states':states,'extend':INDEX_EXTEND},
                              context)






'''
@login_required(login_url='/')
def editentity(request,ide=None):
    context = RequestContext(request)
    forms_list = [{'fieldname':'phone','label':'Teléfono','plhdr':'(xx-xxxxxxx)'},
                  {'fieldname':'email','label':'Email','plhdr':'email@domine.something'},
                  {'fieldname':'url','label':'Sitio Web','plhdr':'http://www.domine'}
    ]
    try: 
        myme = Contact.objects.get(userrelated=request.user.pk)
    except:
        myme = request.user

    estados = Sepomex.objects.all().distinct('estado')
    #SE TRATA DE UNA EDICION DE DATOS
    ent = Entity.objects.get(pk=ide)
    if ent.estadoa:
        delomuns = Sepomex.objects.filter(estado=ent.estadoa).distinct('municipio')
    else:
        delomuns = None
    if ent.delomun:
        colonias = Sepomex.objects.filter(municipio=ent.delomun).distinct('asentamiento')
    emails = EmailList.objects.all().distinct()
    return render_to_response('forms/edit_entity_form.html',
                             {'lista':forms_list,
                             'ent':ent,'myme':myme,
                             'estados':estados,
                             'delomuns':delomuns,
                             'colonias':colonias,
                             'emails':emails
                             }
                             ,context)
'''
