# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, permission_required
import simplejson
from django.contrib.auth import authenticate,login as auth_login,logout
from contactmanager.forms import *
from appentity.models import *
from django.conf import settings
from appcontact.restviews import tagging
@login_required(login_url="/")
def checkcontact(request,contacto):
        emailcheck= EmailList.objects.filter(email=contacto)
        if not emailcheck:
            response={'r':'newc'}
        else:
            response={'r':'checked'}

        return HttpResponse(simplejson.dumps(response))

@login_required(login_url="/")
def addphoto(request):
    if request.POST:
        context = RequestContext(request)
        fields = ['entity_file']
        response = {}
        forma = FormCreator()
        modelo = EntityFile
        data = request.POST.copy()
        files = request.FILES
        response['entity_file']='%sentityFiles/%s'%(settings.MEDIA_URL,str(files['entity_file']))
        formaldeido = forma.advanced_form_to_model(modelo,fields)
        formaldeido = formaldeido(data,request.FILES)
        if formaldeido.is_valid():
            newphotoc = EntityFile(entity_file=request.FILES['entity_file'])
            newphotoc.save()
            response['pk']=newphotoc.pk
            return HttpResponse(simplejson.dumps(response))
            #return render_to_response('pices/euploaded.html',{'data':response},context)
        else:
            return HttpResponse('no se subira una foto para el usuario seleccionado')
    else:
        return HttpResponse()


def delphoto(request,idu):
    context = RequestContext(request)
    if idu:
        p = EntityFile.objects.get(pk=idu).delete()
    response = {'del':True}
    return HttpResponse(simplejson.dumps(response))

@login_required(login_url="/")
def addentity(request):
    return HttpResponse('') 

@login_required(login_url="/")
def myentities(request):
    return HttpResponse('') 



@login_required(login_url="/")
def relcontacts(request):
    vals = []
    contactos_to_add = request.GET.getlist('contacto')
    entidad = request.GET.get('entity',None)
    if entidad:
        entidad = Entity.objects.get(pk=entidad)
    else:
        return HttpResponse('No existe esa organizacion')
    for c2add in contactos_to_add:
        if c2add:
            try: emails = EmailList.objects.get(email=c2add)
            except: emails = None
            if emails:
                contactando = emails.contacto
                imagex = emails.contacto.photoimg()
                if imagex:
                    imagex = u'/static/%s'%imagex.contact_file
                    print imagex
                else: 
                    imagex = '/static/images/nophoto.png'
            else:
                contactando = Contact.objects.create(namec='%s'%c2add,usercreator=request.user.pk,status=0)
                imagex = '/static/images/nophoto.png'
            
            vals.append({'pk':contactando.pk,'namec':contactando.namec,'email':c2add,'photox':imagex,'ent':entidad.pk,'status':contactando.status})
            em,emfail = EmailList.objects.get_or_create(email=c2add,contacto=contactando)
            #newemail = EmailList(email=c2add,contacto=contactando)
            
            okc,failc = As_Contact.objects.get_or_create(asociation=entidad,contacto=contactando) 

    return HttpResponse(simplejson.dumps(vals))

def removerel(request,idc=None,ide=None):
    try:
        ac = As_Contact.objects.get(contacto=idc,asociation=ide)
        ac.delete()
    except:
        ac = None
    response = simplejson.dumps({'delete':'ookk'})
    return HttpResponse(response)
           
    
@login_required(login_url="/")
def oldaddentity(request):
    response = {}
    fields = ['user','nameac','addressac','cpac','twitteract',
              'extra','phonesac','urlsac','photoa','estadoa','delomun','colonia','latac','lonac']
    forma = FormCreator()
    modelo = Entity
    data = request.POST.copy()
    data['user']=request.user.pk
    phones = request.POST.getlist('phone')
    phonestypes = request.POST.getlist('typeofphone')
    contactos_to_add = request.POST.getlist('contacto')
    
    urls = request.POST.getlist('url')
    urlstypes = request.POST.getlist('typeofurl')
    phoneList = [
        {'phone':x,
        'typePhone':phonestypes[phones.index(x)],
        } for x in phones]
    urlList = [
        {'url':x,
        'typeofUrl':urlstypes[urls.index(x)],
        } for x in urls]

    phonesJson = simplejson.dumps(phoneList)
    urlsJson = simplejson.dumps(urlList)
    data['phonesac']=phonesJson
    data['urlsac'] = urlsJson
    
    if data['id']:
        instanced = Entity.objects.get(pk=data['id'])
    else:
        instanced = None
    formaldeido = forma.advanced_form_to_model(modelo,fields)
    formaldeido = formaldeido(data,instance=instanced)
    if formaldeido.is_valid():
        datar = formaldeido.save()
        if datar.pk:
            '''
            for c2add in contactos_to_add:
                emails = EmailList.objects.filter(email=c2add)
                if emails:
                    contactando = emails[0].contacto
                else:
                    contactando = Contact(namec='Noname',usercreator=request.user)
                    contactando.save()
                
                newemail = EmailList(email=c2add,contacto=contactando)
                okc,failc = As_Contact.objects.get_or_create(asociation=datar,contacto=contactando) 
            '''
            formater = FormatingData()
            emails = request.POST.getlist('email',None)
            typeofemails = request.POST.getlist('typeofemail')
            params = [emails,typeofemails]
            rup = formater.saveListEmaiLEntity(params,datar)
            response['data']=data 
            response['saved']=True
            response['pk']=datar.pk
            return HttpResponse(simplejson.dumps(response))
    else:
        response['saved']='faild'
        response['err']=formaldeido.errors
        return HttpResponse(simplejson.dumps(response))

    return HttpResponse(simplejson.dumps(response))


def addingMeta(params=None):
    if params:
        for p in params:
            m,fail = MetaEntity.objects.get_or_create(
                                                asignetstring=p['strings'],
                                                asignetto=p['asignedto'],
                                                metastring=p['metastring'],
                                                metavalue = p['metavalue'],
                                                contact_meta = p['contact']
                     
                        )
        return True
    else:
        return False




@login_required(login_url="/")
def addentity(request):

    idc = request.POST.get('id',None)
    if idc:

        instance = Entity.objects.get(pk=idc)
        m = MetaEntity.objects.filter(contact_meta=instance)
        m.delete()
    else:
        instance = None

    data = request.POST.copy()
    modelo = Entity
    modelform = FormCreator()
    modelform = modelform.form_to_model(modelo)
    modelform = modelform(data,instance=instance)
    extras = ExtraFields.objects.all().order_by('ordering') 
    extraform = DynamicForm()
    extraform = extraform.from_model(extras)
    tags = request.POST.getlist('tags')
    redsocial = request.POST.getlist('redsocial')
    redsociallist = request.POST.getlist('redsociallist')
    direccion =  request.POST.getlist('direccion')
    wdir = request.POST.getlist('whereisdir')
    data['user'] = request.user.pk
    data['status']='1'
    data['tags']=None
    #data['date_create']=datetime.datetime.now()
    errors = {}
    success = {}
    m = None

    if modelform.is_valid():
        m = modelform.save()
        success.update({'modelform':'ok','pk':m.pk})
        for t in tags:
            tagging(t,None,m.pk)
        if extraform:
            extraform = extraform(data)
            if extraform.is_valid() and m:
                pices = [ {'strings':'entity',
                       'asignedto':x.name.replace('q_',''),
                       'metastring':u'%s'%x.label,
                       'metavalue':u'%s'%x.value(),
                       'contact':m } for x in extraform ]
                metas = addingMeta(pices)
                success.update({'extra':'ok'})
            else:
                errors.update(extraform.errors)

        contactlist = request.POST.getlist('contactinfo')
        typeof = request.POST.getlist('typeof')
        whereis = request.POST.getlist('whereis')
        piceofsh = []
        
        for singlec in contactlist:
            if singlec:
                singlec = singlec
                pices_contact = [{'strings':'entity',
                              'asignedto':100,
                              'metastring':u'%s'%typeof[contactlist.index(singlec)],
                              'metavalue':u'%s|%s'%(singlec,whereis[contactlist.index(singlec)]),
                              'contact':m }] 
                piceofsh.append(pices_contact)
            else:
                pices_contact = None
            if pices_contact:
                metas = addingMeta(pices_contact)

    else:
        errors.update(modelform.errors)
    rss = []
    for rs in redsocial:
        if rs:
            rsdict = [{'strings':'redsocial',
                       'asignedto':1,
                       'metastring':u'%s'%redsociallist[redsocial.index(rs)],
                       'metavalue':u'%s'%rs,
                       'contact':m
                    }]
            metas = addingMeta(rsdict)

    for d in direccion:
        if d:
            rsdict = [{'strings':'direccion',
                       'asignedto':1,
                       'metastring':u'%s'%wdir[direccion.index(d)],
                       'metavalue':u'%s'%d,
                       'contact':m
                    }]
            metas = addingMeta(rsdict)


       
  
    if errors:
        response = simplejson.dumps(errors)
    else:
        success['redirect']='myentities'
        response = simplejson.dumps(success)
    return HttpResponse(response)





