# -*- encoding: utf-8 -*-
from django.forms import ModelForm
import simplejson
from sitio.models import *
from django import forms



class FormCreator:
    def __init__(self):
        pass

    def form_to_model(self,modelo):
        if modelo: 
            meta=type('Meta',(),{'model':modelo})
            crearForm=type('modelform',(ModelForm,),{"Meta":meta})
        return crearForm

    def advanced_form_to_model(self,modelo,fields=None,widgets=None,excludes=None):
        meta = type('Meta',(),{"model":modelo,'fields':fields,'widgets':widgets,'exclude':excludes})
        KtemodelfomrKlss = type('modelform',(ModelForm,),{"Meta":meta})
        return KtemodelfomrKlss


class FormatingData:
    def __init__(self):
        pass

    def list_params(self,params):
        myname = params[0]
        myvalues = params[1]
        typesof = params[2]
        listing = []
        for x in myvalues:
            if x:
                listing.append({myname:x,
                'type'+str(myname):typesof[myvalues.index(x)],
                })

        return simplejson.dumps(listing)

    def saveListRelated(self,params,targeted):
        myvalues = params[0]
        mytypes = params[1]
        listing = [{'email':x,
                 'typeofemail':mytypes[myvalues.index(x)]} for x in myvalues]
        EmailList.objects.filter(contacto=targeted.pk).delete()
        for x in listing:
            if x['email']:
                emailsaver= EmailList(contacto=targeted,email=x['email'],typeofemail=x['typeofemail'])
                emailsaver.save()
 
        return listing

    def saveListEmaiLEntity(self,params,targeted):
        myvalues = params[0]
        mytypes = params[1]
        listing = [{'email':x,
                 'typeofemail':mytypes[myvalues.index(x)]} for x in myvalues]
        EntityEmailList.objects.filter(entity_id=targeted.pk).delete()
        for x in listing:
            if x['email']:
                emailsaver= EntityEmailList(entity_id=targeted,email=x['email'],typeofemail=x['typeofemail'])
                emailsaver.save()
 
        return listing


            
#FIELDS_LIST = {
#                'open':forms.CharField('label')
#              }


class Formcontact(forms.Form):
    CHOICES_CONTACT = [('Email','Email'),
                        ('Tel. Móvil','Tel. Móvil'),
                        ('Tel. Fijo','Tel. Fijo'),
                        ('Tel. Recados','Tel. Recados'),
                        ]
    CHOICES_WHERE = [('Trabajo','Trabajo'),
                     ('Casa','Casa'),
                     ('Otro','Otro')
                    ]
    CHOICES_REDSOCIAL = [('Facebook','Facebook'),
                         ('Twitter','Twitter'),
    
                        ]

    typeof = forms.ChoiceField(label=u'Forma de contactar',choices=CHOICES_CONTACT)
    contactinfo = forms.CharField(label=u'',max_length=200)
    whereis = forms.ChoiceField(label=u'',choices=CHOICES_WHERE) 
    redsociallist = forms.ChoiceField(choices=CHOICES_REDSOCIAL)
    redsocial = forms.CharField(max_length=200)
    direccion = forms.CharField(max_length=200)
    whereisdir = forms.ChoiceField(label=u'',choices=CHOICES_WHERE)




