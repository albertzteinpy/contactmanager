from django.contrib import admin
from sitio.models import *
from django.db.models import get_app, get_models
    
app = get_app('sitio')
models = get_models(app)
for m in models:
    classname = u'%sAdmin'%m._meta.verbose_name
    model = m
    SuperClass = type(str(classname),(admin.ModelAdmin,),{"model":model})
    admin.site.register(model,SuperClass)
