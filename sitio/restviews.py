# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, permission_required
import simplejson
from django.contrib.auth import authenticate,login as auth_login,logout
from sitio.models import *
from django.conf import settings
import datetime

def login(request):
    usr = request.POST.get("user")
    pwd = request.POST.get("pass")
    user = authenticate(username=usr, password=pwd)
    if user:
        if user.is_active:
            auth_login(request,user)
            response = {'l':'ok','redirect':'inicio'}
    else:
        response = {'l':'fail'}
    
    return HttpResponse(simplejson.dumps(response))


