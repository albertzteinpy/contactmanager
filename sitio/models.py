# -*- encoding: utf-8 -*-i
from django.db import models
from django.contrib.auth.models import User 

SALESPOINT_STATUS = (
        (1,'Activado'),
        (2,'Desactivado')
    )

CUSTOMER_STATUS = (
        ('1','Prospecto'),
        ('2','Cliente'),
        ('3','Comprador'),
        ('4','Extra')
    )


class SalesPoint(models.Model):
    class Meta:
        verbose_name = u'Punto de Venta'

    pointname = models.CharField(u'Punto de Venta',max_length=200)
    zipcode = models.CharField(u'C.P.',max_length=12)
    address = models.CharField(u'Calle/número',max_length=200)
    colonia = models.CharField(u'Colonia',max_length=200)
    delomun = models.CharField(u'Delegación/Municipio', max_length=200)
    state = models.CharField(u'Estado',max_length=200)
    status = models.BooleanField(default=1,choices=SALESPOINT_STATUS)
    poligon = models.TextField(blank=True,null=True)

    def __unicode__(self):
        return u'%s'%self.pointname



class Seller(models.Model):
    class Meta:
        verbose_name = u'Vendedores'
    salespoint = models.ForeignKey(SalesPoint)
    fullname = models.CharField(u'Nombre',max_length=200)
    birthdate = models.DateField(u'Fecha de Nacieminto')
    email = models.EmailField(u'eMail',max_length=254)
    phone = models.CharField(u'Tel.',max_length=20)
    sellerzipcode = models.CharField(u'C.P.',max_length=20)
    userid = models.ForeignKey(User,verbose_name='Usuario')
    status = models.BooleanField(default=1)

    def __unicode__(self):
        return u'%s'%self.fullname




class Customer(models.Model):
    class Meta:
        verbose_name = 'Cliente'

    c_fullname = models.CharField(u'Nombre Completo',max_length=250)
    birthdate = models.IntegerField(u'Año de nacimiento')
    contacto = models.TextField(blank=True,null=True)
    czipcode = models.CharField(u'c.p.',max_length=20)
    addr = models.CharField(u'Dirección',max_length=250)
    c_colonia = models.CharField(u'Colonia',max_length=200)
    c_delomun = models.CharField(u'Delegación/Municipio', max_length=200)
    c_state = models.CharField(u'Estado',max_length=200)
    seller_related = models.ForeignKey(Seller,blank=True,null=True)
    status = models.CharField(u'Status',max_length=20,choices=CUSTOMER_STATUS)
    date_create = models.DateTimeField(auto_now_add=True) 
    last_update = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '%s'%self.c_fullname



class Product(models.Model):
    class Meta:
        verbose_name = u'Producto'

    p_name = models.CharField(u'Producto',max_length=200)
    sales_point = models.ManyToManyField(SalesPoint)
    price = models.DecimalField(u'Precio',max_digits=12, decimal_places=2,null=True,blank=True)
    photo = models.ImageField(upload_to='product',blank=True,null=True)

    def __unicode__(self):
        return u'%s'%self.p_name

SALES_STATUS = (
                ('1','Proceso'),
                ('2','Concretada'),
                ('3','Cancelada'),
                ('4','Devolucion Producto')
                )

class Sale(models.Model):
    class Meta:
        verbose_name = u'Venta'

    sellerid = models.ForeignKey(Seller,verbose_name = 'Vendedor')
    sales_point_sale = models.ForeignKey(SalesPoint,verbose_name = 'Punto de Venta')
    customer_buy = models.ForeignKey(Customer, verbose_name = 'Comprador')
    product_saled = models.ForeignKey(Product)
    amount_sale = models.DecimalField(u'Cantidad',max_digits=20,decimal_places=2)
    quantity = models.IntegerField(u'Q',default=1)
    saledate = models.DateTimeField(auto_now = True)
    sale_status = models.CharField(max_length=20,choices = SALES_STATUS)
    
    def __unicode__(self):
        return u'%s %s %s'%(self.sellerid,self.customer_buy,self.product_saled)
