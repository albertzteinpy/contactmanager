# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.context import RequestContext
from django.shortcuts import get_object_or_404, render_to_response,render
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import redirect
from django.contrib.auth import logout
import string as STrg
from django.db.models import Q
from salespoint.forms import * 
import datetime

ESTADOS = [(u'Aguascalientes', u'Aguascalientes'), (u'Baja California', u'Baja California'), (u'Baja California Sur', u'Baja California Sur'), (u'Campeche', u'Campeche'), (u'Chiapas', u'Chiapas'), (u'Chihuahua', u'Chihuahua'), (u'Coahuila de Zaragoza', u'Coahuila de Zaragoza'), (u'Colima', u'Colima'), (u'Distrito Federal', u'Distrito Federal'), (u'Durango', u'Durango'), (u'Guanajuato', u'Guanajuato'), (u'Guerrero', u'Guerrero'), (u'Hidalgo', u'Hidalgo'), (u'Jalisco', u'Jalisco'), (u'M\xe9xico', u'M\xe9xico'), (u'Michoac\xe1n de Ocampo', u'Michoac\xe1n de Ocampo'), (u'Morelos', u'Morelos'), (u'Nayarit', u'Nayarit'), (u'Nuevo Le\xf3n', u'Nuevo Le\xf3n'), (u'Oaxaca', u'Oaxaca'), (u'Puebla', u'Puebla'), (u'Quer\xe9taro', u'Quer\xe9taro'), (u'Quintana Roo', u'Quintana Roo'), (u'San Luis Potos\xed', u'San Luis Potos\xed'), (u'Sinaloa', u'Sinaloa'), (u'Sonora', u'Sonora'), (u'Tabasco', u'Tabasco'), (u'Tamaulipas', u'Tamaulipas'), (u'Tlaxcala', u'Tlaxcala'), (u'Veracruz de Ignacio de la Llave', u'Veracruz de Ignacio de la Llave'), (u'Yucat\xe1n', u'Yucat\xe1n'), (u'Zacatecas', u'Zacatecas')]



def years_to_cur():
    final = datetime.datetime.now().year
    inicio = datetime.datetime.now().year-100
    years = [(0,'----seleccione---')]
    for i in range(inicio,final):
        years.append((i,i),)
    return years

def logoff(request):
    logout(request)
    return redirect('/')


def index(request):
    context = RequestContext(request)
    return render_to_response("inicio.html",{},context)


def inicio(request):
    forma = FormCreator()
    modelo = Customer
    args={}
    widgets = {
                'birthdate':forms.Select(choices=years_to_cur()),
                'c_state':forms.Select(choices=ESTADOS,attrs={'class':'form-control'}),
                'c_delomun':forms.Select(),
                'c_colonia':forms.Select(),
                'zipcode':forms.Select()
              }
 
    forma = forma.advanced_form_to_model(modelo=Customer,widgets=widgets)

    # ARGS SECTION
    args['forma']=forma
    return render(request,'startpage.html',args)
