var emails = [];
function cloning()
{
    var parentCloner = $(this).parents("li:first");
    clone = $(parentCloner).clone();
    $(clone).find(".adder").click(cloning);
    $(clone).find(".deler").click(deler);
    $(clone).find('input').val('');
    $(clone).find('.void,.v_email').focus(clean_the_house);
    $(clone).find('.v_phone').keyme();
    $(clone).find('.ui-selectmenu-button').remove();
    $(clone).find('.selector').attr('id','').selectmenu();
    $(clone).find('.ui-selectmenu-button').css('width','65px');
    $(clone).find('.v_contacto').autocomplete({source:emails});
    $(parentCloner).after(clone);
}

function deler()
{
    var parentToDel = $(this).parents("li:first");
    items = $(this).attr("href").replace('#remover','');
    if($("[name="+items+"]").length>1)
        $(parentToDel).remove();
    else
        $(parentToDel).find("input").val('');
}


function submitPhoto(){
    if($('#contact_file').val().length>1)
    {
        $('.loadphoto').text('Cargando, por favor espere un momento...').show();
        dadd = $(this).parents("form:first");
        $(dadd).attr('action','/addphoto').attr('target','nopest');
        $(dadd).submit();
        return false;
    }
    else
    {
        alert('Debe seleccionar una imagen');
        $('#contact_file').click();
        return false;
    }
}

function submitEPhoto(){
    if($('#entity_file').val().length>1)
    {
        $('.loadphoto').text('Cargando, por favor espere un momento...').show();
        dadd = $(this).parents("form:first");
        $(dadd).attr('action','/addphotoentity').attr('target','nopest');
        $(dadd).submit();
        return false;
    }
    else
    {
        alert('Debe seleccionar una imagen');
        $('#entity_file').click();
        return false;
    }
}


function delPhoto(){
    var victim = $(this).attr('href').replace('#','');
    
    if(victim!='/delphoto//' && confirm('¿Esta seguro que desea eliminar la foto?'))
    {
        $.ajax({url:victim,dataType:'json',success:function(response){
                $('.myphoto').attr('src','/static/images/nophoto.jpg');
                $('#photoc').val('');
            }
        });    
    }
    return false;
    
}

var err_validation = {};

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
};

function validator()
{
    $('.err').remove();
    $(".void").each(function(){
            if($(this).val().length<=0)
            {
                $(this).after('<span class="inline_blocke err">debe contener algo de información.</span>');
            }

    });
    $('.v_email').each(function(){
            if(!isValidEmailAddress($(this).val()))
            {
                $(this).after('<span class="inline_blocke err">El dato email no es correcto.</span>');
            }
        
    });

    return $('.err').length;
}

function clean_the_house(){
    vno = $(this).parents('li:first').find('.err');    
    $(vno).remove();
}

function sendPost()
{
        if($(this).attr("action")=='/addphoto' || $(this).attr("action")=='/addphotoentity'){
            return true;
        }
        else{
            var validated = validator();   
            var actionsef = $(this).attr('action');
            if(validated==0)
            {
                data = $(this).serializeArray();
                $.ajax({url:'/'+actionsef,
                    data:data,
                    type:'post',
                    dataType:'json',
                    success:function(response){
                        
                        if(actionsef=='addentity')
                            details = '/detailentity/';
                        else
                            details = '/detailcontact/';
                        
                        if(response.saved==true)
                            window.location=details+response.pk+'/'
                        $("#id").val(response.pk);
                    }
                });
            }
        return false;
        }
    
}


function srchandshow()
{
    myvalue = $(this).val();
    myvalidated = isValidEmailAddress(myvalue);
    if(myvalidated)
    {
        $.ajax({url:'check/'+myvalue,dataType:'json',success:function(response){
            }
        
        });
    }
}


function restColonias()
{
    data = {'pk':$(this).val()};
    $.ajax({url:'/colonia',
            type:'get',
            dataType:'json',
            data:data,
            success:function(response){
                var whereis = $('#tocolonia');
                datas = response;
                parseLocations(whereis,datas,'colonia');
            }
    });

}


function parseLocations(whereis,datas,nameson)
{
    $('#'+nameson).remove();
    var selector = document.createElement('select');
    selector.name = nameson;
    selector.id = nameson;
    $(selector).append('<option value="">Seleccione</option>');
    $.each(datas,function(x,y){
        $(selector).append('<option value="'+y.municipio+'">'+y.municipio+'</option>');    
    });
    $(whereis).after(selector);
    if(nameson=='delomun' && location.pathname.indexOf('edtcontact')=='-1')
    $(selector).selectmenu({change:restColonias});
    if(nameson=='delomun' && location.pathname.indexOf('edtcontact')>'-1'){
        $(selector).val($('#otherdelomun').val());    
        $(selector).selectmenu({create:restColonias,change:restColonias});
    }
    if(nameson=='colonia' && location.pathname.indexOf('edtcontact')>'-1')
    {
        $(selector).val($('#othercolonia').val());
        $(selector).selectmenu();
    }

    if(nameson=='colonia' && location.pathname.indexOf('edtcontact'=='-1'))
        $(selector).selectmenu({change:localizeme});
}


function restLocation()
{
    data = {'pk':$(this).val()};
    $.ajax({url:'/location',
            type:'get',
            dataType:'json',
            data:data,
            success:function(response){
                var whereis = $('#tomunicipio');
                datas = response;
                parseLocations(whereis,datas,'delomun');
            }
    });

}




var tags=[];
function tagger()
{
    myvalue = $(this).attr('href').replace('#','');
    if(tags.indexOf(myvalue)<0)
        tags.push(myvalue);
    $('.tagging').remove();
    $.each(tags,function(x,y){
            var ah=document.createElement('a');
                $(ah).addClass("glyphicon glyphicon-remove-sign movert");
                $(ah).click(function(){
                    tags.splice(tags.indexOf(y),1);
                    $(this).parents('div:first').remove();    
                });
                $(ah).attr('href','#').html('&nbsp;');
                var taginput=document.createElement('input');
                    $(taginput).attr('type','hidden').attr('name','tag');
                    $(taginput).val(y);
                    
                var divbox = document.createElement('div');
                    $(divbox).addClass('tagging').append(taginput);
            $('#buscando').before(divbox);
    });
    $('#search_form').attr('action',location).submit();

}

function chowinput()
{
   var victim = $(this).attr('href');
   $(victim).show();

}



function removetag()
{
   $(this).parents('li:first').remove();
   var who = $(this).attr('href').replace('#','').replace('tag_','');
   $("[value='"+who+"']").remove();
   $('#'+who).val('');
   $('#search_form').attr('action',location).submit();
}

$(document).ready(function(){
    $('.removetagg').click(removetag);
    $(".adder").click(cloning);    
    $(".deler").click(deler);
    $('.void,.v_email').focus(clean_the_house);
    $("#addcontact,#addentity").submit(sendPost);
    $("#addphoto").click(function(){
        $("#contact_file").click();    
    });
    $("#addephoto").click(submitEPhoto);
    $('.delphoto').click(delPhoto);
    $("#btn_save").click(function(){
        $(this).parents("form:first").attr('action',$(this).parents("form:first").attr('id'));    
    });
    $('.v_contactoa').keyup(srchandshow);
    $('.v_phone').keyme();
    $("#contact_file").change(submitPhoto);
    $("#entity_file").change(submitEPhoto);
    $( ".selector" ).selectmenu();
   if(location.pathname.indexOf('edtcontact')=='-1')
   {    
        $('.location_drop').selectmenu({
            change:restLocation
        });
    }

    $('.tagger').click(tagger);
    
   // TO SEARCH PANEL ----------------------------------------------------------------------------

    $('.chowinput').click(function(){

        $('#search_form').attr('action',location);
        var victim = $(this).attr('href');
        var whereami = $(this).position();
        var topp = $(this).height()+whereami.top+15;
        $('.tab_s').addClass('blind');
        $('#boxy_float').css('left',whereami.left+'px').css('top',topp+'px').removeClass('blind');
        $(victim).removeClass('blind');
        $('#buscando').removeClass('blind');  
    });

    $('.locatio_drop').change(restLocation);
    
    $('.mover').click(function(){
        var victim = $(this).attr('href');
        $(victim).find('input:first').val('');
        $(victim).addClass('blind');
    });
    $('.movert').click(function(){
        $(this).parents('div:first').remove();    
    });

    $('.v_contacto').autocomplete({
        source:emails    
    });

    $('.closer').click(function(){
        var who_close = $(this).attr('href');
        $(who_close).addClass('blind');
    });


});
