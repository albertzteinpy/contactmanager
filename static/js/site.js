
function remove_err(){
        $(this).siblings('.err:first').remove();   
}



function sendit(e)
{
    e.preventDefault();
    // EXTRACTING DE FORM DATA
    var data = $(this).serializeArray();
    var url = $(this).attr('action');
    var method_is = $(this).attr('method');

    $.ajax({url:url,
            type:method_is,
            data:data,
            dataType:'json',
            success:function(response){
                $('.err').remove();
                $.each(response,function(x,y){
                    if(response.redirect)
                    {
                        window.location.href = '/'+response.redirect;
                    }
                    else
                    {
                        $('#id_'+x).after('<span class=" inline_blocke err">'+y+'</span>').change(remove_err);
                    }
                });
                $('.err:first').siblings().focus();
            }
    });
}


