$( document ).ready(function(){
	$( "#process" ).submit(function() {
	    data = $(this).serializeArray();
	    data.push({'name': 'elslug', 'value':slug})
	    $.ajax({
	        data:data,
	    	url:'/registerprocessajax/',
	        type:'post',
	        dataType:'json',
	        success:function(data){
	        	console.log(data)
	        	$('.lista_procesos').empty();
	        	$.each(data, function(i,valor){
	        		$('.lista_procesos').append('<li><a href="#">'+valor['fields']['name']+'</a></li>')
	        	})
	        	$('#AddProceso').modal('hide');
	        }
	    });
	return false;
	});
});