$( document ).ready(function() {
    $('.popover-dismiss').popover({
	  trigger: 'focus'
	})

	$( "#id_Datehechos" ).datepicker({
     	dateFormat: 'dd/mm/yy',
		yearRange: "1920:2014",
		dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Jue', 'Vie', 'S&aacute;b'],
		changeMonth: true,
		changeYear: true,
	});
	$( "#id_Datelegal" ).datepicker({
		dateFormat: 'dd/mm/yy',
			yearRange: "1920:2014",
			dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Jue', 'Vie', 'S&aacute;b'],
			changeMonth: true,
			changeYear: true,
	});
	$( "#id_Dategire" ).datepicker({
		dateFormat: 'dd/mm/yy',
			yearRange: "1920:2014",
			dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Jue', 'Vie', 'S&aacute;b'],
			changeMonth: true,
			changeYear: true,
	});
	$( "#fechaCierre" ).datepicker({
		dateFormat: 'dd/mm/yy',
			yearRange: "1920:2014",
			dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Jue', 'Vie', 'S&aacute;b'],
			changeMonth: true,
			changeYear: true,
	});
	$( "#fechaPosible" ).datepicker({
		dateFormat: 'dd/mm/yy',
			yearRange: "1920:2014",
			dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mi&eacute;', 'Jue', 'Vie', 'S&aacute;b'],
			changeMonth: true,
			changeYear: true,
	});
	 $('.descripcion').readmore({
	 	moreLink: '<a href="#">Leer Más</a>',
	 	lessLink: '<a href="#">Cerrar</a>',
	 	maxHeight: 40
	});
	$('#id_Timehechos').datetimepicker({
		datepicker:false,
		format:'H:i',
		step:10
	});
	
	
});
